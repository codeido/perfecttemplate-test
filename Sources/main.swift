//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import Foundation

// Initialize base-level services
PerfectServer.initializeServices()

// Create our webroot
// This will serve all static content by default
let webRoot = "./webroot"
try Dir(webRoot).create()

// Add our routes and such
// Register your own routes and handlers
Routing.Routes["/"] = {
    request, response in
    
    var session = URLSession.shared
    var request = URLRequest(url: URL(string: "https://api.forecast.io/forecast/b6f4ced31c5f980e2b37a6427ba3b8f6/37.8267,-122.423")!)
    var task = session.dataTask(with: request as URLRequest){
        (data, response2, error) -> Void in
        if error != nil {
            response.appendBody(string: "<html><title>ERROR!</title><body>ERROR!</body></html>")
            response.completed()
        } else {
            var result = NSString(data: data!, encoding:
                String.Encoding.ascii.rawValue)!
            response.appendBody(string: "<html><title>\(result)!</title><body>\(result)</body></html>")
            response.completed()
        }
    }
    task.resume()
    
    
}

do {
    
    // Launch the HTTP server on port 8181
    try HTTPServer(documentRoot: webRoot).start(port: 8181)
    
} catch PerfectError.networkError(let err, let msg) {
    print("Network error thrown: \(err) \(msg)")
}
